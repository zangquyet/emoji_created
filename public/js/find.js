$(document).ready(function() {
	$("#find").submit(function(event) {
		if ($("#key").val()==="") {
			toastr.warning('Keyword is required');
			$("#key").focus();
			return false;
		}
		return true;
	});
});