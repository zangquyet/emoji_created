<?php 
/**
* 
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Page extends CI_Controller
{
	public function __construct(){
		parent::__construct();
	}
	function recently()
	{
		$this->load->library('cookie');
		$num_per_page=10;
		$page=!$this->input->get('page')?1:$this->input->get('page');
		$listid=$this->cookie->getlist();
		$listid=explode('-', $listid);
		if (count($listid)<=1) {
			$listid=array(0);
		}
		array_pop($listid);
		$offset=($page-1)*$num_per_page;

		$total_record=count($listid);
		$total_page=floor($total_record/$num_per_page)+1;
		$phantrang="";
		if ($total_page==1) {
			$phantrang="";
		}else if ($page>$total_page) {
			redirect(base_url("index.php/page/recently?page={$total_page}"));
		}else{
			$phantrang="
			<nav>
				<ul class=\"pagination\">";
					for ($i=1; $i <=$total_page ; $i++) { 
						if ($page==$i) {
							$phantrang.="<li class=\"active\"><a href=\"#\">{$i}</a></li>";
						}else{
							$url="index.php/page/recently?page={$i}";
							$url=base_url($url);
							//die($url);
							$phantrang.="<li><a href=\"{$url}\">{$i}</a></li>";

						}
					}

					$phantrang.="</ul>
				</nav>

				";
			}


			$this->load->model('Emoji');
			$emo=$this->Emoji->getlistcookie($listid,20,$offset);
			
			$data['subview']='page/index';
			$data['title']=CUSTOM_TEXT.' Recently Coppied';
			$data['desc']=CUSTOM_TEXT.'Recently Coppied contains previous copied posts';
			$data['data']=array(
				'emojis'=>$emo,
				'pagination'=>$phantrang
				);
			$data['active']=2;
			$this->load->view('master', $data);
		}
		public function index()
		{
			$this->load->model('Emoji');
			$this->load->library('pagination');
			$config['base_url'] = base_url('index.php/page/index');
			$key="";
			$config['total_rows'] = $this->Emoji->total_record($key);
			$config['per_page'] = 10;

			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Last';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = '&gt;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = '&lt;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			


			$this->pagination->create_links();
			$this->pagination->initialize($config);
			


			
			$list=$this->Emoji->getlist($key,10,$this->uri->segment(3));
			$data['subview']="page/index";
			$data['title']="Home Page";
			$data['desc']=CUSTOM_TEXT." Make emoji, make fun";
			$data['active']=1;
			$data['data']=array(
				'emojis'=>$list,
				'pagination'=>$this->pagination->create_links()
				);
			$this->load->view('master', $data);
		}
		
		public function create()
		{
			$data=array();
			$data['subview']='page/create';
			$data['active']=4;
			$data['title']="Create new emoji";
			$data['desc']=CUSTOM_TEXT." Create new emoji, make new emoji, make new post";
			$data['data']='test';
			$this->load->view('master',$data);
		}
		public function save()
		{	
			if ($this->input->server('REQUEST_METHOD') == 'GET') {
				redirect('page/create', 'refresh');
			}
			/*$this->load->library('session');*/
			$this->load->model('Emoji');

			$this->load->helper(array('form', 'url'));

			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('content', 'Content', 'required',
				array('required' => 'You must provide a %s.')
				);
			//$this->form_validation->set_rules('capcha', 'Captcha', 'callback_captchacheck');
			//$this->form_validation->set_message('captchacheck', 'Captcha is not valid');
			$form=array(
				'title'=>$this->input->post('title'),
				'content'=>$this->input->post('content'),
				'raw_content'=>$this->input->post('raw_content'),
				'create_date'=>date("Y-m-d H:i:s")
				);
			if ($this->form_validation->run() == FALSE)
			{
				
				$data['subview']='page/create';
				$data['active']=4;
				$data['data']=array(
					'error'=>true,
					'errordesc'=>validation_errors(),
					'formdata'=>$form
					);
				$this->load->view('master',$data);
			}
			else
			{
				
				$this->load->library("alias");
				$alias=$this->alias->getalias($this->input->post('title'),array('transliterate' => true));

				$id=$this->Emoji->create($form);
				$data_update=array(
					'url_slug'=>$alias."-".$id
					);
				if ($this->Emoji->update($data_update,$id)) {
					redirect('/page/i/'.$data_update['url_slug']);
				}
			}

		}
		function captchacheck($str)
		{
			return $this->session->captcha==$str;
		}

		public function i($alias)
		{
			$id=explode("-", $alias);
			$id=end($id);
			$this->load->model('Emoji');

			$emoji=$this->Emoji->get_emoji($id);
			if (count($emoji)) {
				
			$data=array();
			$data['subview']='page/page';
			$data['active']=5;
			$data['title']=CUSTOM_TEXT." ".$emoji[0]['title']." ".CUSTOM_TEXT;
			$data['desc']=CUSTOM_TEXT." ".$emoji[0]['title']." ".CUSTOM_TEXT;

			$data['data']=array(
				'emo'=>$emoji[0],
				);

			$this->load->view('master',$data);
			}else{
				$this->load->view('page/remove');
			}
		}
		function random()
		{

			$this->load->model('Emoji');
			$record= $this->Emoji->random();
			$record=$record[0];
			$data['subview']="page/random";
			$data['active']=3;
			$data['title']="Random Emoji";
			$data['desc']=CUSTOM_TEXT."Random Emoji, Happy emoji";
			$data['data']=array(
				'title'=>$record->title,
				'content'=>$record->content,
				'create_date'=>$record->create_date,
				'raw_content'=>$record->raw_content
				);
			$this->load->view('master', $data);

		}
/*		function edit($alias)
		{
			$id=explode("-", $alias);
			$id=end($id);
			$this->load->model('Emoji');

			$emo=$this->Emoji->get_emoji($id);


			$data['subview']="page/edit";
			$data['data']=array(
				'emoji'=>$emo[0]
				);
			$data['active']=6;


			$this->load->view('master', $data);
		}*/
		public function contact()
		{
			$this->load->view('blank_page');
		}
		function search()
		{
			$this->load->model('Emoji');
			$key=$this->input->get('key');
			$cur_page=!$this->input->get('page')?1:$this->input->get('page');
			$total_record=$this->Emoji->total_record($key);
			$total_page=floor($total_record/10)+1;
			$phantrang="";
			if ($total_page==1) {
				$phantrang="";
			}else if ($cur_page>$total_page) {
				redirect(base_url("index.php/page/search?key={$key}&page={$total_page}"));
			}else{
				$phantrang="
				<nav>
					<ul class=\"pagination\">";
						for ($i=1; $i <=$total_page ; $i++) { 
							if ($cur_page==$i) {
								$phantrang.="<li class=\"active\"><a href=\"#\">{$i}</a></li>";
							}else{
								$url="index.php/page/search?key={$key}&page={$i}";
								$url=base_url($url);
							//die($url);
								$phantrang.="<li><a href=\"{$url}\">{$i}</a></li>";

							}
						}

						$phantrang.="</ul>
					</nav>

					";
				}
				$emoji=$this->Emoji->getlistbypage($key,20,$cur_page);
				$data['subview']="page/search";
				$data['data']=array(
					'emojis'=>$emoji,
					'total'=>$total_record,
					'pagination'=>$phantrang
					);
				$data['active']=7;
				$data['title']="Search result for: {$key} ";
				$data['desc']=CUSTOM_TEXT."Search result for: {$key} ";
				$this->load->view('master', $data);
			}

		}
		?>