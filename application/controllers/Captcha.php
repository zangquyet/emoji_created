<?php
class Captcha extends CI_Controller {
    function __construct()
    {
        parent::__construct();
    }
    function index(){    
        $words = preg_split('//', 'abcdefghijklmnopqrstuvwxyz0123456789', -1);
        shuffle($words);
        $words=implode('', $words);
        $words=substr($words,0,5);

        $vals = array(
            'word'          => $words,
            'img_path'      => './public/captcha/',
            'img_url'       => base_url('public/captcha/'),
            'font_path'     => './public/font/captcha.ttf',
            'img_width'     => '150',
            'img_height'    => 30,
            'expiration'    => 7200,
            'word_length'   => 8,
            'font_size'     => 16,
            'img_id'        => 'Imageid',
            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

        // White background and border, black text and red grid
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
                )
            );

        $cap = create_captcha($vals);
        $this->session->set_userdata('captcha', $words);
        echo $cap['image'];
    }           
} 
?>  