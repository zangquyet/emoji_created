<?php $this->load->view('partial/header'); ?>





<div class="container-fluid" style="background:#f3f3f3;">
<div class="container">
	<div class="row">
	
<div class="col-md-12">	
<div class="col-md-7" style="background:#f3f3f3; padding:10px;">
<ul class="nav nav-pills bmd-nav-pills-default">
    <li class="<?= $active==1?"active":"" ?>"><a href="<?= base_url() ?>" class="bmd-ripple">Newest</a></li>
  <li class="<?= $active==2?"active":"" ?>"><a href="<?= base_url("page/recently") ?>" class="bmd-ripple">Recently Copied</a></li>
  <li class="<?= $active==3?"active":"" ?>"><a href="<?= base_url("page/random") ?>" class="bmd-ripple">Random</a></li>
  <li class="<?= $active==4?"active":"" ?>"><a href="<?= base_url("page/create") ?>" class="bmd-ripple">Create</a></li>
  
</ul>
</div>
<div class="col-md-5" style="text-align:Center; padding:7px; background:#f3f3f3;">	

<form id="find" class="form-inline" action="<?= base_url("index.php/page/search") ?>" >
  <div class="form-group">
    <div class="input-group">
      <input type="text" class="form-control input-lg" value="<?= $this->input->get("key") ?>"  style="border:1px solid #d4d6d0;" id="key" placeholder="Type your keyword" name="key">
    </div>
  </div>
 <button type="submit" class="btn btn-warning bmd-ripple btn-lg">Search</button>
</form>
</div>

	</div>
	</div>
</div>
</div>



<div class="container">
	<div class="row">

	<div class="col-md-8" style="margin-bottom:30px;">
	

	
		

	<div class="row">
	

<?php $this->load->view($subview,$data); ?>


	
	</div>

		
		
	</div>


	<div class="col-md-4" style="border:solid 1px #ccc; text-align:center">
	Empty DIV
	</div>

	</div>

	
</div>



<?php $this->load->view('partial/footer'); ?>
