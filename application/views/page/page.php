<script>
   $(document).ready(function() {
    var clipboard = new Clipboard('.btn');
   });
</script>

<div class="panel panel-default" style="text-align:left;">

  <div class="panel-heading">
    <h3 class="panel-title"><?= $emo['title'] ?></h3>
	<small>Created on <?= $emo['create_date']?></small>
  </div>
  <div class="panel-body text-left">
    <?= $emo['content'] ?>
	
  </div>
  
  <div class="col-md-12" style="text-align:center; background:#eee; padding:10px; margin-top:10px;">

  
  <a onclick="copy(<?= $emo['id'] ?>)" data-clipboard-text="<?= $emo['raw_content'] ?>" class="btn btn-default bmd-ripple b  bmd-ink-grey-400 btn-hide0"><b>COPY</b></a>

 </div>

</div>
