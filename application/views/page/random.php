  
<script>
 $(document).ready(function() {
     var clipboard = new Clipboard('.btn');
   clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
 });
</script>



<div class="col-md-12" style="margin-top:20px;">
			<blockquote>
				<p><?= $title ?></p>
				<p><small>Posted on <?= $create_date ?></small></p>
				<div>
					<?= $content ?>
				</div>
				<div class="home-btn">
				<a href="javascript:void(0)" data-clipboard-text="<?= $raw_content ?>" class="btn btn-default bmd-ripple b  bmd-ink-grey-400 btn-hide0"><b>COPY</b></a>
					<ul class="pager bmd-state-default">
  <li><a href="" class="bmd-ripple">Previous</a></li>
    <li><a href="" class="bmd-ripple">Next</a></li>

  </ul>
				</div>
			</blockquote>
		</div>