	
<script>
	$(document).ready(function() {
		var clipboard = new Clipboard('.btn');
	});
</script>


<div class="col-md-12" style="margin-top:20px;">
	Showing results for <strong><?= $this->input->get("key") ?></strong>.
</div>
<?php if (count($emojis)): ?>
	<?php foreach ($emojis as $emo): ?>

		<div class="col-md-12" style="margin-top:20px;">
			<blockquote>
				<p><a href="<?= base_url('i/'.$emo->url_slug) ?>"><?= $emo->title ?></a></p>
				<p><small>Created on <?= $emo->create_date ?></small></p>
				<div>
					<?= $emo->content ?>
				</div>
				<div class="home-btn">
					<a onclick="copy(<?= $emo->id ?>)" data-clipboard-text="<?= $emo->raw_content ?>" class="btn btn-default bmd-ripple-only  btn-hide0"><b>COPY</b> </a>
					
				</div>
			</blockquote>
		</div>
	<?php endforeach ?>
<?php else: ?>
	<div class="col-md-12" style="margin-top:20px;">
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			No results found for <strong><?= $this->input->get("key") ?></strong>. <a href="<?= base_url('page/create') ?>"><h3>Create One</h3></a>
		</div>
	</div>
<?php endif ?>
<?= $pagination ?>