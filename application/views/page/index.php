	
<script>
	 $(document).ready(function() {
	 	var clipboard = new Clipboard('.btn');
	 });
</script>
<?php if (count($emojis)): ?>
	<?php foreach ($emojis as $emo): ?>

		<div class="col-md-12" style="margin-top:20px;">
			<blockquote>
				<p><a href="<?= base_url('i/'.$emo->url_slug) ?>"><?= $emo->title ?></a></p>
				<p><small>Posted on <?= $emo->create_date ?></small></p>
				<div>
					<?= $emo->content ?>
				</div>
				<div class="home-btn">
				<a onclick="copy(<?= $emo->id ?>)" data-clipboard-text="<?= $emo->raw_content ?>" class="btn btn-default bmd-ripple-only  btn-hide"><b>COPY</b> </a>
					
				</div>
			</blockquote>
		</div>
	<?php endforeach ?>
<?php else: ?>
	<div class="col-md-12" style="margin-top:20px;">
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Ops , Not found</strong> We can't find any emoji
		</div>
	</div>
<?php endif ?>
<div class="text-center">
	<?= $pagination ?>
</div>