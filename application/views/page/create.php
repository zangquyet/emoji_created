<script type="text/javascript">
  function capcha() {
    $.ajax({
      url: '<?= base_url('captcha') ?>',
      type: 'GET',
      dataType: 'html',
    })
    .done(function(result) {
      $("#capcha").html(result);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  }

  capcha();
  $(document).ready(function() {
    var hand=$("#example1").emojioneArea({
      autoHideFilters: false
    }
    );
    $("#form").submit(function(event) {
      $("#result").val($(".emojionearea-editor").html());
      var content=$("#example1").val();
      if (content=="") {
        $(".emojionearea-editor").focus();
        toastr.error('Emoji is required!');
        return false;
      }
      return true;
    });
  });

</script>
<?php if (isset($error)&&$error): ?>
  <script>
    $(document).ready(function() {
      $("#btn-shw").click();
    });
  </script>
<?php endif ?>
<form id="form" method="POST" action="<?= base_url('index.php/page/save') ?>">
  <div class="col-md-12" style="text-align:Center;">
  
    <?php if (isset($error)&&$error): ?>
      <div class="alert alert-warning" style="margin-top:20px;text-align: left;">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong><?= $errordesc ?></strong> 
    </div>
  <?php endif ?>
  <div class="panel panel-default" >
    <div class="panel-body text-left" style="padding: 0px">
      <textarea name="raw_content" id="example1" style="display: none"><?= isset($error)&&$error?$formdata['raw_content']:"" ?></textarea>
    </div>

  </div>
  <div class="p-start" style="padding-top:10px;">



    <a href="javascript:void(0)" class="btn btn-warning  bmd-ripple b bmd-floating bmd-ink-grey-400" id="btn-shw"><b>CREATE</b></a>

  </div>
  <div class="p-end">
    <div class="form-group has-info padup">
      <div class="bmd-field-group">
        <input minlength="5" maxlength="60" value="<?= isset($error)&&$error?$formdata['title']:"" ?>" type="text" name="title" class="bmd-input"/>
        <span class="bmd-bar"></span>
        <input type="hidden" name="content" id="result">
        <label class="bmd-label">Enter a Title</label>
        <span class="bmd-field-feedback"></span>
      </div>
     <!--  <div id="capcha" style="margin-top: 10px"></div>
     <div class="bmd-field-group">
       <input type="text" name="capcha" class="bmd-input" required=""/>
       <span class="bmd-bar"></span>
       <label class="bmd-label">Captcha</label>
       <span class="bmd-field-feedback"></span>
     </div> -->
    </div>
    <button type="submit" class="btn btn-warning bmd-ripple">CREATE</button>
  </div>
</div>
</form>
<div style="padding-bottom: 700px"></div>