<!DOCTYPE html>
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?= isset($desc)?" - ".$desc:"" ?>">
	<title><?= isset($title)?$title:"" ?></title>
	<link rel="stylesheet" href="<?php echo base_url('public/dist/emojionearea.min.css');?>">
	
	<link rel="stylesheet" href="<?php echo base_url('public/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/css/bootstrap-md.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('public/dist/sprite.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/style.css'); ?>"/>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
	
	<!-- Latest compiled and minified CSS & JS -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="<?php echo base_url('public/js/clipboard.js');?>"></script>
	<script src="<?php echo base_url('public/js/bootstrap-md.min.js'); ?>"></script>
	<script src="<?php echo base_url('public/js/activebtn.js'); ?>"></script>
	<script src="<?php echo base_url('public/js/copy.js');?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<script src="<?php echo base_url('public/js/find.js');?>"></script>
	<script src="<?php echo base_url('public/js/prettify.js');?>"></script>

	<script src="<?php echo base_url('public/dist/emojionearea.js');?>"></script>
	<style>
		.emojionearea-picker.emojionearea-picker-position-top.emojionearea-filters-position-top.hidden{
			display: block!important;	
			margin-top: 230px;
		}
		.emojionearea .emojionearea-picker.hidden{
			width: 100%;
		}
		.emojionearea .emojionearea-picker.emojionearea-filters-position-top .emojionearea-filters{
			width: 100%;
		}
		.emojionearea .emojionearea-picker .emojionearea-wrapper{
			width: 100%;
		}
		.emojionearea .emojionearea-picker .emojionearea-scroll-area{
			width: 100%;
		}
		.emojionearea .emojionearea-picker .emojionearea-scroll-area{
			width: 100%;
		}
	</style>
	<script>
		$(document).ready(function() {
			$("emojionearea-picker").removeClass("hidden");
		});
	</script>
</head>
<body>

	<div class="container-fluid mobile-hide" style="background-color:#544e6b; height:52px; ">
		<div class="container" style="padding:0px; ">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-header" >
						<a class="navbar-brand bmd-ripple" href="<?= base_url() ?>">Emoji</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
						<ul class="nav navbar-nav navbar-right">
							<li class="active"><a href="<?= base_url('page/contact') ?>" class="bmd-ripple">Contact</a></li>


						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>

