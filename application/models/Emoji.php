<?php 
class Emoji extends CI_Model {

        public $table="emoji";
        public function __construct(){
                parent::__construct();
                // Your own constructor code
        }

        function total_record($key)
        {
            $this->db->like('title',$key);
            $this->db->from($this->table);
            return $this->db->count_all_results();
        }
        function create($data){
           $this->db->insert($this->table, $data);
           $insert_id = $this->db->insert_id();
           return  $insert_id;
        }
        function update($data,$id){
         $this->db->where("id", $id);
                return $this->db->update($this->table, $data);
        }
        function get_emoji($id)
        {       
                $this->db->where('id', $id);
                $q = $this->db->get($this->table);
                return $q->result_array();
        }
        function random()
        {
                $this->db->from($this->table);
                $this->db->order_by("RAND()")->limit(1);
                $query = $this->db->get(); 
                return $query->result(); 
        }
        function getlist($key,$offset,$limit)
        {

            $this->db->like('title',$key);
            $this->db->from($this->table);
            $this->db->order_by('create_date',"desc");
            $this->db->limit($offset,$limit);
            $query = $this->db->get(); 
            return $query->result(); 
        }
        function getlistcookie($array,$offset,$limit)
        {
            $arr=array_slice($array, $limit,$offset);
            $list=array();
            foreach ($arr as $id) {

                $this->db->where('id',$id);
                $this->db->from($this->table);
                $record=$this->db->count_all_results();

                if ($record!=0) {
                    $this->db->where('id', $id);
                    $this->db->from($this->table);
                    $query = $this->db->get(); 
                    $kq=$query->result();
                    $list[]=$kq[0]; 
                }
                
            }
            return $list;
        }
        function getlistbypage($key,$offset,$limit)
        {
            $off=($limit-1)*$offset;
            $this->db->like('title',$key);
            $this->db->from($this->table);
            $this->db->order_by('create_date',"desc");
            $this->db->limit($offset,$off);
            $query = $this->db->get(); 
            return $query->result(); 
        }
}   
?>