<?php
/**
 * summary
 */
class Cookie
{
	function getlist()
	{
		if(!isset($_COOKIE['listid'])) {
			return "";
		} else {
			return $_COOKIE['listid'];
		}
	}
	public function setid($id)
	{
		$list=array();
		$list=$this->getlist();
		$list=explode('-', $list);
		if (!in_array($id, $list)) {
			$list[]=$id;
		}
		$list=implode('-', $list);
		setcookie('listid', $list, time() + (86400 * 30*30), "/");

	}
}
?>